# frozen_string_literal: true

require_relative './html_app'
require_relative './api'

use Rack::Deflater

run Rack::Cascade.new [Searcher::API, Searcher::WebApp]

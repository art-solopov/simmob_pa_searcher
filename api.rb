# frozen_string_literal: true

require 'grape'

require_relative './lib/query'

module Searcher
  class API < Grape::API
    format :json
    prefix :api

    get '/search' do
      query = Query.for_phone_arena(params[:q], links_limit: 1) do |q|
        q.attribute :name, '//body//div[@id="phone"]/h1/span[1]'
        q.attribute :dimensions, '//body//div[@id="phone_specificatons"]' \
                                 '//li[@class="s_lv_1"]/' \
                                 'strong[starts-with(text(), "Dimensions")]' \
                                 '/../ul/li'
      end
      query.execute
    end
  end
end

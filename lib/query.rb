# frozen_string_literal: true

require 'faraday'
require 'nokogiri'

class Query
  attr_accessor :host,
                :search_path,
                :search_params,
                :links_xpath,
                :links_limit

  attr_reader :attributes

  def self.for_phone_arena(search, **options, &block)
    new(
      host: 'http://www.phonearena.com',
      search_path: '/search',
      search_params: { term: search },
      links_xpath: '//body//div[@id="phones"]/div[@class="s_listing"]' \
                   '/div/a[@class="s_thumb"]/@href',
      **options,
      &block
    )
  end

  def initialize(**options)
    @attributes = []
    options.each { |k, v| send(:"#{k}=", v) }
    yield self if block_given?
  end

  def attribute(field_name, field_xpath, coercion = nil)
    @attributes << Attribute.new(field_name, field_xpath, coercion)
  end

  def execute
    links = search_doc.xpath(links_xpath).take(links_limit).map(&:to_s)
    links.map do |link|
      # TODO: what if the link is absolute?..
      doc = fetch_link(link)
      attributes.reduce({}) { |s, a| s.merge(a.value(doc)) }
    end
  end

  private

  def search_doc
    search = conn.get(search_path, search_params)
    Nokogiri::HTML(search.body)
  end

  def fetch_link(link)
    res = conn.get(link)
    Nokogiri::HTML(res.body)
  end

  def conn
    @conn ||= Faraday.new(url: @host)
  end

  class Attribute
    COERCIONS = {
      integer: ->(e) { Integer(e.match(/\d+/)[0]) },
      string: Proc.new(&:to_s)
    }.freeze

    def initialize(field_name, field_xpath, coercion = nil)
      @field_name = field_name
      @field_xpath = field_xpath
      @coercion = coercion
    end

    def value(doc)
      value = doc.xpath(@field_xpath).text.strip
      coercion = case @coercion
                 when Proc then @coercion
                 when ->(x) { COERCIONS.key?(x) } then COERCIONS[@coercion]
                 else ->(x) { x }
                 end

      { @field_name => coercion.call(value) }
    end
  end
end

/* globals require */

const path = require('path')
const process = require('process')

const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const addSrc = require('gulp-add-src');
const rev = require('gulp-rev');
const gulpIf = require('gulp-if');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');

const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')

const mergeStream = require('merge-stream');

const isProd = process.env['APP_ENV'] === 'production'

function node_module(...pathComponents) {
    return path.join('.', 'node_modules', ...pathComponents)
}

function jsStream() {
    const addJS = [
        node_module('jquery', 'dist', 'jquery.js'),
        node_module('bootstrap', 'dist', 'js', 'bootstrap.js'),
        node_module('handlebars', 'dist', 'handlebars.js')
    ]
    
    return gulp.src('assets/js/**/*.js')
        .pipe(babel({presets: ['env']}))
        .pipe(addSrc.prepend(addJS))
        .pipe(concat('app.js'))
        .pipe(gulpIf(isProd, uglify()))
        .pipe(rev())
        .pipe(gulp.dest('public/assets/js'))
}

function cssStream() {
    const addCSS = [
        node_module('bootstrap', 'dist', 'css', 'bootstrap.css')
    ]

    const CSSPlugins = [
        autoprefixer({ browsers: '> 1%' })
    ]
    
    return gulp.src('assets/css/**/*.css')
        .pipe(postcss(CSSPlugins))
        .pipe(addSrc.prepend(addCSS))
        .pipe(concat('app.css'))
        .pipe(gulpIf(isProd, postcss([cssnano()])))
        .pipe(rev())
        .pipe(gulp.dest('public/assets/css'))
}

function fontsStream() {
    return gulp.src([
        node_module('bootstrap', 'dist', 'fonts', '*')
    ])
        .pipe(gulp.dest('public/assets/fonts'))
}

gulp.task('default', () => {
    return mergeStream(jsStream(), cssStream(), fontsStream())
        .pipe(rev.manifest({path: 'assets-manifest.json'}))
        .pipe(gulp.dest('./'))
})

gulp.task('lint', () => {
    const eslint = require('gulp-eslint')
    const stylelint = require('gulp-stylelint')

    const jsLint= gulp.src('assets/js/**/*.js')
          .pipe(eslint())
          .pipe(eslint.format())

    const styleLint = gulp.src('assets/css/**/*.css')
          .pipe(stylelint({
              reporters: [
                  {formatter: 'string', console: true}
              ]
          }))

    return mergeStream(jsLint, styleLint)
})

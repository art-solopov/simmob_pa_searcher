/* global $, Handlebars */

$(() => {
    const $el = $('#value')

    const source = $('#value_template').html()
    const template = Handlebars.compile(source)

    const spinner = $('<span>').addClass('glyphicon glyphicon-repeat spinner')
    
    $('#search_form').submit(e => {
        e.preventDefault()
        $el.html(spinner)
        $.ajax(
            '/api/search',
            {
                method: 'get',
                data: { q: $('#q_search').val() }
            }
        ).done(res => {
            $el.html(template(res))
        })
    })
})

module.exports = {
    parserOptions: {
        ecmaVersion: 6
    },
    env: { browser: true },
    extends: 'eslint:recommended',
    rules: {
        eqeqeq: "warn",
        indent: ['error', 4]
    }
}

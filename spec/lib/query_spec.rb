# frozen_string_literal: true

require_relative '../../lib/query'

RSpec.describe Query, vcr: true do
  subject do
    described_class.for_phone_arena('nokia') do |q|
      q.links_limit = 3
      q.attribute :name, '//body//div[@id="phone"]/h1/span[1]'
    end
  end

  let(:names) do
    ['Nokia Mural', 'Nokia Lumia 1020', 'Nokia Lumia 1520'].map do |n|
      { name: n }
    end
  end

  it { expect(subject.execute).to eq names }
end

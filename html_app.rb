# frozen_string_literal: true

require 'json'

require 'sinatra/base'
require 'haml'

Haml::Options.defaults[:format] = :html5

module Searcher
  class WebApp < Sinatra::Base
    get '/' do
      @manifest = JSON.parse File.read('assets-manifest.json')
      haml :index
    end
  end
end
